「⋯⋯總而言之！我們就是不服！沒錯吧，瓦薩！？」
「沒錯！就是不服！」

兩人對著我大喊大叫起來
當然，羅蕾露也聽地很清楚
我們互看一眼，彼此的心情都能明白
因為是羅蕾露，就算戴著面具，也能看懂我的表情吧
總而言之，看來談話是進行不下去了

「⋯⋯那麼，該怎麼辦呢？」

老人這樣向瓦薩和芙安娜問到，他們則毫不猶豫地答道

「讓我和打敗老頭子的人戰鬥！如果我們贏了，就帶你們到《長》那裡去！」
「我們贏了的話，你們就到此為止了！爺爺你也一樣呢！」

老人深深地嘆了口氣

「⋯⋯呵。知道了，隨你們喜歡⋯⋯就是這麼回事了，你們也好好加油吧」

認可了二人的發言後，他又轉身對我們鼓勵了一句
恐怕老人早就猜到會是這種發展了吧
因為對他們二人的性格了如指掌嘛
我有些在意的向老人問到

「⋯⋯被你栽培上來的傢伙，都是這種直腸子的笨蛋嗎？」

若真是那樣，這一路上豈不是白擔心了

《哥布林》雖然老成穩重，但《塞壬》就相當冒失。而眼前的兩個人，比起《塞壬》更是有過之而無不及，因為根本不聽人說話呢
聽到我的問題，老人搖了搖頭

「不可能那樣吧，這兩個傢伙比較特殊而已。其它還有很多人都出去做任務了，不在據點。只是這兩個笨蛋因為不適合纖細的工作，所以待在據點的時間比較多」

似乎是個家裏蹲二人組哎

「不過，也不是就讓他們一直無所事事的，沒有任務的時候，就讓他們做些管理闘技場之類的事務性工作。現在一定很閑吧，離闘技大會還很遠，近期也沒有什麼特別的活動計劃。闘技場就這麼一直空閑著，所以吶⋯⋯」

他們會特意來找茬，說不定也是為了消磨時間吧

《哥布林》含糊的透露了這層意思
雖然覺得這理由也太奇葩了點。但正因為無所事事，才會主動找茬吧，這樣想來也能夠理解
總而言之，現在已經無法避免戰鬥了
我和羅蕾露再次互看一眼，點了點頭

「⋯⋯知道了！就來戰吧！哪個是我的對手？」

我回答了瓦薩和芙安娜
當然，最好是讓我去對付瓦薩，羅蕾露對付芙安娜
但他們二人感覺有點小孩子脾氣，這邊提出要求，說不定會被故意反對
而把選擇權交給對方的話，對魔術有興趣的芙安娜應該會擅自選擇羅蕾露，剩下的瓦薩自然就會對上我，這樣就正中下懷
老人這時又出言推了他們一把

「啊，說起來，羅蕾露雖然釋放了驚人的魔術，但還不至於讓老朽昏過去。打出最後一擊的，是這邊的雷特呢，而且還是用劍⋯⋯」

一聽到這話

「那麼，我來和你打！雷特！」

瓦薩這樣喊道
而芙安娜則是

「等等！為什麼擅自就決定了啊！」

雖然被抱怨了，但瓦薩說到

「這樣也沒什麼不好的吧，你不是也說過，對他們的魔術有興趣嗎？如果傳言沒錯的話，她可是能連發比你還厲害的魔法呢⋯⋯雖然我覺得不可能就是了」
「唔──嗯⋯⋯嘛，沒錯！那我就選這個女人！可以吧！」

馬上中了圈套的二人
雖然芙安娜自稱《魔賢》，但真的有「賢」的成分嗎，著實可疑
不過，事情能按計劃發展，對我們毫無疑問是有利的
因此，我也不會有什麼意見

「知道了，就這麼辦吧」

我這樣答道


◇◆◇◆◇

詢問了如何進行比試的問題，自然是使用這個地下闘技場

據說，在這個地下闘技場⋯⋯當然上面的闘技場也有，為了不讓攻擊魔術傷及觀眾，設置有阻斷攻擊的魔法盾，因此能夠放開手腳進行戰鬥

至於操縱人員，只要是組織成員都沒問題，這次就由《哥布林》主動承擔起了操縱任務
因為如果有個萬一，老人打算用實力強迫瓦薩和芙安娜協助，所以不能離開闘技場太遠

當然，他們二人對此一無所知
芙安娜在觀眾席上期待著接下來的比賽，而瓦薩則站在我面前，舉起了短槍
看樣子他是個槍術家
與他相對的我，則抽出了單手劍

不是之前用的那把，而是換了一把魔力和聖氣都無法通過的劍。據說魔術很難對他奏效，所以不使用魔力也可以吧
而且，我也想隱藏起聖氣
王牌可不能隨便就拿出來用呢
之前對老人用過了，但那是沒辦法的情況

雖然現在也不是很有餘裕，可正因為沒有餘裕，才更要謹小慎微，隱藏起自己的手牌，盡可能努力吧
若是判斷失誤，快要被幹掉了的時候，就不管這麼多了

「⋯⋯劍士嗎？那種像小針一樣的劍，怎麼可能讓老爺子昏過去」

瓦薩對我說道
這麼說也沒錯，我的劍在那種巨體面前，確實和小針區別不大
根據扎到的地方不同，說不定就像是做針灸呢
不過很遺憾，我確實是用這柄劍把老人打昏過去了

「即使是這樣小針，刺在身上也會很痛哦。要試試看嗎？」

唇槍舌戰⋯⋯雖然有點不符合我的性格
但也差不多吧

「哈」

被對方用鼻子嗤笑了⋯⋯嘛，算了

「那麼，要宣布比賽開始了，都準備好了嗎？」

老人像裁判一樣站在我和瓦薩中間，對我們這樣說到
裁判的人選只剩下他了呢
羅蕾露也在，但畢竟是我們這邊的人，有偏袒我的可能性
老人雖被他們當作叛徒，但瓦薩卻說「老爺子不是會在這種地方作弊的人」，真是奇怪的信賴呢，不過能同意就好
聽到老人的話，我和瓦薩點了點頭，老人舉起了手

「那麼⋯⋯開始！」

說著把手揮下，宣告了模擬戰的開始