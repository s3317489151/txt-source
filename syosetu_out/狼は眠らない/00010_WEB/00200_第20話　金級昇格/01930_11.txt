在領主館，特斯拉隊長來迎接了。

沃卡城的守護隊，分成擔當城門的隊，擔當領主館的隊，以及擔當城鎮治安的隊，三隊各有隊長。特斯拉隊長，是擔當領主館的隊長。
雷肯無言地威嚇正在吼叫的木狼，帶著艾達進了館內。

被帶到的房間，並非領主的職務室，而是擺設了高級器具的接待室。
說了去沙發坐，就坐下了。
不久後，領主來了。兒子阿基托也一起來了。

「呀，雷肯。好久不見」
「啊啊」
「這位就是艾達嗎」
「是」
「原來是如此惹人憐愛的少女嗎。多指教了。這次的盜賊討伐，辛苦妳了」
「謝，謝謝」
「那麼，雷肯。想推薦你為金級冒険者。能接受嗎」
「成為金級冒険者，會有什麼義務嗎」
「沒什麼特別的義務。老夫或城鎮的有力人士說不定會提出指名委託，但拒絶了也無所謂。也就是說，不論有沒有成為金級，作為冒険者的立場都不會改變」
「在希拉那裡的藥師修行，會在今年結束。之後應該會到各地流浪。還沒有具體的預定就是了」
「這樣阿。那真是可惜。但是，也不是完全不會回到這城鎮吧？」
「不知道。可能會回來。可能不會」
「那就好。可以的話就時不時回城鎮吧。不過那也是看你的打算」
「我成了這城鎮的金級冒険者，對你來說有什麼好處？」
「呼嗯。單是城鎮有金級冒険者一事，治安就會變好。然後，有著能讓周圍的領主和打著壊注意的人，在想找麻煩時猶豫的效果。而且你的場合是特別的」
「特別是指？」
「領主要威懾城鎮內外時，擁有騎士團是最好的，但這城鎮還沒達到那階段。迷宮都市的領主自然很能威懾。畢竟隨時都能僱用高位冒険者。而對不屬於這些的領主來說，金級冒険者能作為其代替」
「原來如此」
「但是你跟艾達，在身為金級冒険者的同時，也是迷宮踏破者。而且你是踏破了兩個迷宮共計三次的，世上少有的冒険者。這樣的你，這樣的你們，是這城鎮的冒険者。要說這不特別，那又要怎麼比喻呢」
「我們今後，不保證會照你的意志行動。就算如此，成為金級也有意義嗎」
「那種事不是問題。應該說，要是得行使你們這武力，那就是政治上的敗北。抑制力要不去使用，才有其意義在」
「呼嗯？複雜的話聽不太懂。總而言之，我們不需要背負任何義務，而對你來說有利益的話，那就好」
「能這麼說就幫大忙了。雖然有些失禮，但根據以前見到你時的印象，也做了今天的商量是不是會很困難的覺悟」
「因為希拉跟我說了些話」
「希拉殿？說了什麼？」
「說是跟你的對談，能讓的就全部都讓出去」
「這樣阿。希拉殿這麼講了嗎。對她，真的是」

真的是，怎麼樣。領主沒有說出後續。
經過了一陣子的沉默後，雷肯開了口。

「如果就這點事的話，我們就回去了」
「阿，不，等等。還有事想商量」
「說吧」
「首先，想再確認一次。你，單獨踏破了貢布爾迷宮對吧」
「啊啊」
「而且是，兩次」
「啊啊」
「難道說，你之前來這裡時，就已經踏破迷宮了嗎？」

之前是什麼時候來領主館的，雷肯搜尋著記憶。

「第一次踏破，然後回到這城鎮，是在那一天吧。你的兒子破壊了希拉家的門，侵入進去，打算逮捕我」
「那個就別說了。有覺得很抱歉。不過，是嗎。在那時候，你就已經是迷宮踏破者了嗎。結果卻那樣對待，老夫跟兒子，無能也該有個限度。拜託你原諒了」

沃卡領主竟然低下了頭。旁邊的兒子也低下了頭。站在牆邊的特斯拉隊長也低下了頭。
只有一瞬，雷肯嘗到了爽快的滋味。在下一瞬間，心裡的警報響起了。

（這城鎮的最大權力者和其繼承人現在對我低了頭）
（我嘗到了勝利的快感）
（但是有什麼不對）
（這對我來說真的是勝利嗎）
（讓他們低頭不會反過來變得很糟糕嗎）

那並非理論，而是直覺。
雷肯在直覺上，從讓這兩人低頭一事，感覺到自己的脖子要被繩子綁住的危機感。
雷肯突然，低下了頭。
雷肯低下的頭，比領主和其兒子還深。

「讓領主和其繼承人蒙羞了。我對此致上歉意」
「雷，雷肯竟然會對人低頭」

旁邊的艾達很驚訝。

「阿，不」

領主也很困惑的樣子。

「今後，如果有事要找我和希拉，希望能派部下過來」
「別這麼說，我也希望今後能拜訪希拉殿和妮姫殿」

雷肯抬起了頭。
領主的兒子阿基托筆直地看著雷肯。

其旁邊的領主，一臉苦澀。

「沒有人能禁止身為領主繼承人的你，拜訪住在這城鎮的希拉吧。但是，庶民有庶民的生活和規則。可以的話，希望能予以尊重」

雷肯全力動著腦，擠出了這台詞。但是，道出的同時，自己也說不上有充分了解自己的這台詞的意義。

「那當然了。今後，是不會擾亂希拉殿和妮姫殿的生活的」

聽到阿基托的話，領主的臉變得更苦澀。

（贏了阿）

是贏在哪裡，怎麼贏的，雷肯自己也不清楚，但直覺上這麼認為。