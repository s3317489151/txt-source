１１

「呼嗯。讓艾達和諾瑪做助手兼隨從嗎。但隨從這邊也會準備喔」
「那些人，應該知道哪邊有什麼東西，能做入浴的準備，也能把垃圾打掃乾淨吧」
「那當然了」
「他們會為希拉泡茶吧」
「當然，會安排很會泡茶的人」
「希拉不喝別人泡的茶。都自己泡」
「什麼」
「而且，希拉不喜歡他人待在身邊。尤其是睡覺時」
「隨從不是他人」
「那是你的想法」
「是沒錯」

跟克里姆斯談話時，雷肯注意到一件事。

對至今忽略了這件事的自己感到生氣。
希拉的外觀雖然是老婆婆，但希拉實際的肉體，年輕得有十八歳左右。

是矇混了外觀，讓自己看起來像老婆婆。
但是，如果觸摸了，又如何。
觸摸了，說不定就會被發現有著年輕的肌膚。

「然後阿。希拉非常討厭被觸摸」
「什麼」
「如果強硬地要去摸，就會發怒跑出去，不再回到這裡吧」
「怎麼會這樣。竟然是那麼奇怪的人嗎」
「因為不普通，才能做到普通人做不到的事」
「原來如此。可能就是那樣吧」
「所以，能直接做身邊的雜事的，只能是艾達或諾瑪」
「那就沒辦法了。嘛，艾達確實是這城鎮最厲害的〈回復〉使，老夫也知道諾瑪這施療師擁有優秀的能力。希拉大人的知己這件事倒不知道」
「為了讓我跟艾達學習〈回復〉的用法，選了諾瑪那裡做研修處的就是希拉。希拉有信賴，尊敬諾瑪到這種程度」
「啊啊，是這樣阿。所以艾達才會在諾瑪那裡進行施療阿。原來如此」

克里姆斯嘆了口氣。

「絲夏娜，不受你中意嗎」
「你很欣賞我，還給了最豐厚的待遇，我很感謝」
「喔」
「但是，冒険者的生存方式不同。我最討厭被束縛。如果有所無禮，就麻煩你原諒了」
「哈哈哈。能讓你說到這地步就有意義了喔。不過。絲夏娜會哭吧」
「什麼」
「提出這次的事的，是絲夏娜」
「你說什麼」
「為了幫助痛苦不已的自己，從天上差遣下來的〈騎著有翼之馬的騎士〉就是你。在那之後憧憬就越來越強烈了喔」
「聽不太懂」
「乙女的夢喔」
「我的身姿和臉，在小女孩眼裡，應該相當可怕吧」
「在大男眼裡也很可怕喔」
「我這時候該笑嗎？」
「笑了就更恐怖了」
「不過，這麼一來，絲夏娜從挺早的時期就知道我的事了嗎？」
「解開那孩子的詛咒時，老夫不能不把傑尼的事說出來。在必要之時，從不知何處入手〈神藥〉，這種事就連王都的大商人也辦不到。絲夏娜想親自跟傑尼道謝」
「原來如此」
「然後傑尼就說，這次是拜大神的引導所賜，講述了只可能是宛如從天派遣下來的騎士的冒険者之活躍」
「我懂了」
「那冒険者輕易地砍倒百隻魔獸，察覺到持有不可思議的武器的四個刺客，並且獨自打倒，最後儘管被同伴背叛，因為卑怯的暗算倒下，仍喚起了不屈的意志，砍下對方的武器，取得了勝利」
「喔。真厲害的傢伙阿」
「絲夏娜以為你是，稀世罕見的英雄。實際上，你是很厲害的人。老夫花了不少時間才注意到這件事。某方面來說，絲夏娜先入為主的想法才是正確的」
「這樣阿」
「但是老夫反倒很佩服傑尼。運氣強到能在那場面入手〈神藥〉，抽到你這張鬼牌。所以才下定了決心，要把札克從這城鎮排除」
「當時，札克是這城鎮的領主家商人代表什麼的對吧」
「在注意到之前，就有好幾個在札克的支配下的商人混了進來，擴大了勢力。沒辦法防止寨卡茲商店鑽進來。札克本人來到這城鎮時，說實話，老夫做了降入柯古陸斯的軍門的覺悟了喔。傑尼也有所失敗。為了保護傑尼，老夫只能讓那傢伙取代傑尼做商人代表」
「但是你賭在傑尼身上了嗎」
「沒錯。還有札克的年齡」
「什麼？」
「意圖用刺客和背叛者搶奪行李，還僱用了三個強力的冒険者想搶奪迷宮品，這無論如何都太露骨了。實際上，失敗又暴露了陰謀，那傢伙和那傢伙的店就不得不從這城鎮撤退。老夫認為，那是高齡帶來的焦躁」
「原來如此」
「只要再忍耐一下，那傢伙就會死去。之後才是真正的勝負」
「呼嗯」
「昨天，拜訪了希拉大人的家」
「什麼」

那麼，就是雷肯回去之後。

「在斯卡拉貝爾導師滯留時，還請到迎賓館留宿，低下頭這麼拜託了」
「怎麼回應」
「得到同意了」
「喔」
「也受這城鎮不少關照，所以只有這次會給你個面子喔。是這麼說的」

（希拉果然）
（打算在這事情結束後從城鎮消失）

「果然要直接拜訪才好吧」
「也是吧」
「這下最大的懸案就解決了」
「所以今天的臉色才好嗎」
「可能喔」