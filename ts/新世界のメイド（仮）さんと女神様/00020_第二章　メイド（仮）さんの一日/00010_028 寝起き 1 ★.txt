在透過窗戶的光芒照射下， 麻理子 的意識漸漸浮現出來。睡醒過來的麻理子 ，想要逃離從左邊感受到的耀眼光芒，咕嚕地向右邊打了一個翻身。

啪嗒！

「唔」

剛剛感覺到胸口上有什麼東西，在那柔軟的衝擊下 麻理子 微微地睜開了眼睛。視野中一片純白。稍微凝視之後，明白了那個是聳立在眼前的白色牆壁以及在那白色山峰之下延展開來的雪白床單。

（啊嘞，這裡是……）

與看慣了的自己房間的牆壁顏色不同， 麻理子迷迷糊糊地想著。就這樣視線開始往左邊移過去，木製的天花板立即映入眼中。不是自己的房間,不過，對那個構造和顏色有印象。那個應該是非常近期看到過的。

（是在哪裡呢）

那樣考慮著的 麻理子 慢慢地再次閉上了眼睛，剛剛還在朝右側臥的身體也漸漸向仰臥的姿態倒了過去。然後，在胸前有什麼東西從右向左發生了移動，感覺上就像流動著一樣。

(啊，又是胸嗎……嗚姆，刺眼)

麻理子想著大半是在夢裡見到的吧。可是，變成仰臥之後，枕頭左邊絢麗的光芒明亮得晃眼。 麻理子提起被子將頭蒙住，再一次向右邊翻了過去。

啪嗒！

這次在感覺到左邊的胸上有著從左向右的流動感之後，右邊的胸感覺上就像是放在了什麼相當柔軟的東西上那樣。

(原來如此。翻身側躺的的話，一邊的胸部滑向另一邊落在上面了，是這樣的感覺嗎？女人的身體也是有各種各樣麻煩的東西啊……。女人的身體……女人的身體!?)

身體再次打起盹兒的麻理子，在頭腦的角落裡開始考慮著事情 。考慮著在自己身上究竟發生了什麼。在想起的一瞬間，眼睛亮了起來

(不是夢，嗎，那個……姆？　那，我現在，為什麼會睡著呢？)

在床單中的身體有變得僵硬起來， 麻理子探索起了記憶。

(和塔莉婭桑進行了談話。到那裡為止都很好。到中途開始有什麼涌了上來，眼淚不停的流出來，被撫摩背，然後……那個以後怎麼了？)

不管如何回憶，到那裡為止記憶就完全斷掉了。

(是一邊哭一邊就睡過去了嗎！？　但，也只能這麼認為了。騙人的吧，是小孩子嗎？那，這裡就是……)

麻理子 戰戰兢兢地從被子裡面露出腦袋向四周張望。不過不僅塔莉婭，連其他人的影子也沒有看到。房間本身也不是塔莉婭的房間，而好象是其他更小的房間。回想起來的話，在 塔莉婭 的房間也沒有看到類似床的東西。

（這是，是在哪裡啊？）

這麼想著慢慢直立起身子坐了起來，重新觀察起房間。因為之前是用睡迷糊腦袋在考慮事情，仔細看的話會發現牆壁和天花板的構造和塔莉婭的房間很是相似，應該是旅店中另外的房間吧。有著六塊榻榻米大小的程度，家具除了麻理子 現在躺的床就只有木製的桌椅了。桌子上面放置了桶與茶杯以及一堆不知道是什麼的布料堆積起來的小山。

被放置在房間角落的床頭旁邊，牆上一米左右的地方有著一扇窗戶，那面牆的對面則有著一扇木製的門扉。而在另一面牆上的則是怎麼看都是隔頁的兩扇不一樣的門。

（那是壁櫥嗎？）

在麻理子這樣想著的時候，掛在肩膀上的薄薄的被子啪刷地滑落下去。 麻理子不自覺地朝向那邊俯視過去。

「咦!?」

到處都有印著的藍色花紋，與旅館的浴衣一模一樣的白色衣服，以及在敞開的胸口處胸部的溝谷映入眼中。

「衣，衣服!?」

慌慌張張地掀開蓋在身上的被子，被睡得亂亂糟糟的衣服以及從中伸出的赤足。對著腰部伸過去的手確認了內褲還是有好好穿著的,不過，長筒襪和吊襪帶卻消失不見了。

「啊，那裡。是那個嗎？」

麻理子想起了剛剛在桌子上看到的布料堆積起來的小山 ，就這樣下床打算去看的時候在腳邊發現了其他的東西。 並排放置在一起的麻理子 穿的編織靴子與沒有印象的拖鞋。總之就這樣將腳伸入拖鞋裡接近了桌子。

那堆布料的小山正如預料的一樣是衣服。眼熟的白色女式無袖貼身襯衫與吊襪帶，長筒襪絲襪和，雖然有印象，但是在來這邊之後沒有直接見到過的ホワイトブリム與白色的胸罩，被乾淨整齊地疊放成了一摞。

(這是，胸罩？　啊！)

（插圖4882f1f8）

麻理子向胸部俯視過去，敞開的胸口向左右兩邊分開仿彿馬上就要溢出來一樣。這是明顯地沒有帶胸罩的狀態。 麻理子 驚慌的將領口合攏拽緊。

（這個是誰給……）

麻理子想像著變成現在這幅樣子的過程，後背竄起了一陣涼意。

(哭累了就睡過去，然後被運送到這裡替換了衣服……。為什麼變成這樣都沒有醒過來啊，我！　真的是變成孩子一樣了不是嗎)

就在這樣抱著頭懊悔的時候，吭吭，響起了敲門的聲音。

「是!?」

反射性的做出應答的麻理子直到這時才注意到自己現在的樣子，對裸露出大腿的衣服下擺急忙的進行了整理。
