克裡特的中心。九尾之影狐的一處據點，加夫對自己部下的戰果十分滿足地點了點頭。

由於連日的任務，那副野性的面容也看得出來些許疲勞，但眼神還是閃閃發光。

「乾得不錯嘛。這樣的話，BOSS應該會滿意吧」

BOSS發出的特別指令。是將多個組織無力化。
要與本來的任務同時進行是非常困難的。而且殲滅對象裡還包含著原本任務中要用到的組織。

狐會暗中行動不髒自己的手。正因用最低限度的人員來執行，秘密才得以保守。
加夫那縝密的計劃結果也不得不變更。

但是，順利做到了。無論是怎樣的犯罪組織都不能無視狐的提案。
將其引誘過來，時而主動出擊，大部分主要組織的主要成員都解決了。

擊潰那些會給組織帶來怎樣的利益加夫雖然不知，恐怕是有某種考量的吧。

這麼一來，加夫，七尾，『盜賊王』加夫・謝恩菲爾達就領先了同期一步。
雖然看見BOSS真顏的時候嚇了一跳，但想成是信賴的證明還算不錯的。

至今完成了各種各樣的任務了。對曾經率領龐大的盜賊團的加夫來說，謀略是他擅長的領域。
但是，現在，加夫感受到了船新的強烈成就感。

到現在為止基本上都沒發生過什麼意外。雖然這也是值得自豪的本領，但隨機應變也是一個能彰顯本領的得分點。
部下之一龍套不配有名字。作為加夫的右腕奔走的男人平聲說道。

「話說啊，不愧是BOSS直屬，太強了。⋯⋯那樣大鬧真是頂不住⋯⋯」

「⋯⋯駕馭奇特之人也是一種氣度。強者或大或小都會有些難應付的」

總算是打了個圓場，作為BOSS直屬被交過來的那群人，就連見識過大量的麻煩人的加夫看來，也是極度蠻不講理的。

獨斷專行當飯吃。說了別殺還是照殺不誤，擅離崗位，一發現拿著寶具的犯罪者就去強搶寶具，簡直為所欲為。
狐本應是秘密組織才對，但從他們的舉止中完全看不出絲毫保密的感覺。真虧他們能維持秘密至今。

至於疑似狐的燈火和疑似狐的《千變萬化》，疑似狐的燈火不做指示外的事情，部下也不聽加夫的話，甚至還收錢智商稅。
疑似狐的《千變萬化》甚至還向敵人自報家門。儘管如此，沒有大鬧一番還算好的了⋯⋯⋯

即使是那樣的人，也有著和加夫的優秀部下同等或以上的強大力量，這世界怕是藥丸。

「不管好壞，瑪娜物質都會深深影響意志強大的人啊⋯⋯」

所以，以領袖氣質和手腕與各國敵對的加夫也變強了。也因此，那群目中無人的傢伙都異常的強大。
意志強大的人，也是難以操縱的人。BOSS居然能那麼自然地將那群很富有個性的人帶來，我真是五體投地。雖說這次事情能順利解決的話面具就會賜給加夫，但實在是沒有自信能搞定那群人。

不管怎麼說，這次加夫他們能如此迅速殲滅犯罪組織都是得力於BOSS的直屬部隊。
那群傢伙不在的話，戰力就不足了。可能還會讓敵人逃掉了吧。

用最低限度的人員想辦法搞定，這是加夫以前就有的壞習慣。

說不定BOSS也看穿了加夫這樣的弱點。下次應該要想出就算來了緊急任務也能應對的計劃吧。

「Plan Ｘ現在交給其他人就可以了吧。Plan Ａ那邊也別疏漏了。這次可是『狐』的大規模作戰啊」

「啊啊，當然的。畢竟也沒收到中止的命令」

加夫和部下都露出了奸笑。
狐發起的事件，大多都是保持主犯不明的情況就不了了之了。但是，這次的計劃不同。

Plan Ａ是至今未有的大規模作戰，是狐的作戰。這次作戰旨在讓狐揚名立萬。
積蓄力量。潛伏在地下的時代要終結了。

這就意味著新的時代要到來了。

突然，想起了曾共同行動過的同伴──以同樣的尾數自豪的《止水》

粗壯的手臂，手指，在顫抖著。這不是在害怕。這是──武者震。

《止水》是個可怕的魔導師。
龐大的魔力加上經驗。有著社會身份，在暗殺人類方面無人能及其後。

強大的魔導師是貴重的存在。故而，加夫和《止水》尾數相同，任務的性質也不一樣。
加夫也是事後才得知，本來，這個計劃最初似乎是預定讓《止水》暗殺皇帝來開幕的。

然而，未戰先挫。《止水》失敗了。

究竟發生了什麼，詳細情況並未告知給加夫。
雖然表面上說是《千變萬化》預防了暗殺這樣，但是那個《千變萬化》是自己人，所以那些情報應該是是摻了水分的吧。

不過，那些都不重要。

「《止水》是失敗了。但是，我可不一樣──他是我們之中最弱的那個」

計劃基本在平穩推進著。也有BOSS的支援。這樣還失敗那也就是──配不上，這麼一回事。

「話說，他們好像要參加武帝祭──」

「內部安排人員都是基本操作了，但想要潛入武帝祭的話，究竟是多久之前就開始計劃的啊──」

著實可怕。即使是相當上位的加夫，至今為止也沒聽說過《千劍》和《燈火騎士團》是同伴之類的消息。

但是，這次暴露出來了。信賴是很容易崩潰的，這可以說是僅有一次的機會也不過分。失敗是絕不容許的。

這時，負責聯絡的部下跑了進來。

「加夫，總部來了聯絡。問Plan Ａ的那個東西怎麼樣了」

真是意想不到的聯絡，加夫不禁皺眉。
Plan Ａ是以某個道具的力量為前提進行的。那是在帝國博物館嚴密看管的東西，奪取那個道具便是計劃的第一階段。

「那個東西⋯⋯？不是應該上報過作戰成功了嗎⋯⋯已經交給BOSS了。情報傳得有點慢啊⋯⋯」

起源於諜報機關的狐在情報方面下了功夫。幾個小時還好說，情報晚了幾天是非常罕見的。
應該也不會是BOSS忘記傳達消息了──。


§　§　§

路克咬緊牙關，後悔地說。

「不行啊⋯⋯克萊伊。不管試了多少次──還是沒法充能啊」

突然伸向這邊的是，以前狐面愛好會給我的刻有奇怪花紋的劍型寶具。

路克是天生的劍士。雖然是劍士，但也並非完全沒有魔力。
他已經將我庫存的劍型寶具都玩過一遍了。而且，不像我老是要去拜託露西婭那樣，大部分的劍型寶具他都有足夠魔力自己充能。

不，那時不管有沒有魔力，他都要鍛鍊到能上手為止。這就是名為路克・賽科爾的男人。

接過伸出的劍，輕輕拔出軽く透かしてみる。
和一般的劍相比較短，劍士拿來當主武器用的話會不太好使的吧。

『大地之鍵』

如果是和博物館差點被偷走的寶具一樣的話，那麼寶具的力量就不太明確了。

被認定為國寶是因為沒有相似的寶具，也有在高等級寶物殿發現的緣故。
基本上寶具的威力與瑪娜物質的濃度──也就是寶物殿的等級成正相關。大部分寶藏獵人不需要效果不明的寶具，所以寶具的博物館以類似的理由收購寶具也是常有的事。

「⋯⋯大地之鍵，大地之鍵啊⋯⋯」

不過，物如其名。能力不明卻有那樣的名字，應該是有什麼理由的。
雖然新聞上沒有登載詳細信息，但我寶具收藏家的血在騷動。

仿彿用尺子量造的直刃也有著圖紋，實在是非常神秘。

路克捏緊拳頭，大聲宣言。

「呃⋯⋯劍還不能充分使用，我還不夠成熟啊。修行不足啊！但是，已經沒人給我斬了！」

「不啊，這是鑰匙啊。不是劍」

「！？」

路克大吃一驚。

你和莉茲大鬧了一場燈火都來抱怨了。大會也快開始了所以請安分一點吧。

我端正姿勢，和路克四目相對，然後一本正經的胡說八道。

「路克，你知道鑰匙需要什麼嗎？答案出乎意料的簡單哦」

他是超一流的劍士，除了常識以外幾乎所有方面都比我要懂得多，不過關於寶具的知識還是我更勝一籌。
路克凝神思索，沒自信地說。

「對鑰匙來說必需的東西⋯⋯鑰匙孔嗎？」

「⋯⋯⋯⋯」

居然把我想說的話說出來了。

我深深歎息試圖蒙混過關。

「也就是說⋯⋯這是通往未來之門的鑰匙啊！要是被罪惡勢力拿到的話那就真的牙白跌死捏」

「什麼⋯⋯竟然是！？罪惡勢力！？罪惡勢力是什麼啊！？」

「那當然⋯⋯是這樣的？⋯⋯沒錯，世界之敵」

為什麼這麼開心哦⋯⋯我這是在敷衍你啊。我只是在Xjb亂說些你會喜歡的話而已啊。
安靜地看著書的露西婭也用關愛傻子的看眼神過來了。

說得太過隨意要是路克當真了那可麻煩了。該怎麼說好呢⋯⋯⋯

我煩惱片刻，戰戰兢兢的說。

「那就是⋯⋯那個啊，自然災害之類的」

「自然⋯⋯災害啊⋯⋯」

「⋯⋯⋯⋯不過，最強的劍士連災害也能斬斷的哦！」

「！！」

一下子失望又一下子驚訝，路克還真是忙呢。
寶具給了我純屬誤會。離開這城市之前得還回去吧。
不過，好不容易得到了寶具，還回去之前就先拿來玩點花樣吧。

我打了個小小的呵欠，默默地把大地之鍵放在露西Ａ婭面前。