在那之後、一周過去了。

感覺到夏季來臨的蟬兒在森林中鳴叫、炎熱的太陽與灼熱之風降臨大地。
絕對不會公開有政治意圖的神前決鬥、正如火如荼的展開。

神前決鬥的場所選在帝國的帝都建國以前就存在的岩山上、經過整修的『空中競技場』、下方莊嚴的皇帝堡壘一覽無遺。
那曾是貴族們的賭場、劍闘士們浴血奮戰的地方、現在則是作為重要文化遺産的競技場。位於中央審判席的樞機主教、南側的國王愛德華、王妃艾莉西亞和王國文官與王國護衛。
北側則是有皇帝阿爾貝托和皇妃愛麗絲。周圍堅固圍牆看台上、坐滿了各貴族們與息息相關的文武百官們。

「沒想到⋯⋯會再次踏入這片土地上」

位於南側競技場內的通道上、夏莉仰望頭上那古老的石製天花板。

帝國的目標是蘇菲和緹歐兩人、那麼王國能派出的代表除了她、夏莉自身以外別無選擇。
然而、這早已不是夏莉一家人的問題了。國王自身的期望避免戰爭⋯⋯一位冒險者肩負國泰民安的重責。
而且、還是在不清楚帝國在這場決鬥中獲勝時還有什麼要求的情況下⋯⋯⋯⋯但、不管如何肯定不是什麼好條件⋯⋯⋯⋯將一個國家捲入的可能性很高、因此迷惘就會敗北。

「但是啊、為什麼要特別跑來帝國呢？聽起來、在國境上決鬥不就好了嗎？」
「誰知道？我怎麼會知道偉大之人的想法呢！」
「從金絲雀閣下的話來看、似乎是那個皇帝不願意再次移動到邊境上的樣子。身為當權者並非不可能的事情」
「說起來、國王陛下的行動也很可疑呢。就算有護衛跟著⋯⋯這麼輕易就進入他國⋯⋯」

在夏莉附近、對謠言雜談很有興趣的蕾亞、庫德、凱爾和阿斯忒里歐。這四人冒險者是夏莉最近經常以指導者身份參與的隊伍。

「但是、沒想到蘇菲醬和緹歐醬有皇室的血統呢」
「而且是這樣的話、那夏莉桑不就是位原貴族、也是皇太子的婚約者吧？想說夏莉桑是打哪來的、這可真是令人意外噎」
「哈哈哈⋯⋯到現在我也很難相信說」
「嗯、人生啊⋯⋯家家有本難念的經」

最後、為了目送將即將登上決鬥場的母親、兩位小公主當事人蘇菲和緹歐等人聚集在一起。

最初、應該按照古老慣例、在國境的平原上進行神前決鬥、不過、阿爾貝托和愛麗絲無視了那個慣例、拒絕長時間移動。
然而、金絲雀與愛德華卻出乎意料的放縱了他們。由於有金絲雀的秘術空間轉移魔術、王國方面也沒有花費太多的時間在移動、最終以帝國單方面招待國王與王妃這種形式解決了、不過、王國方面的文武百官倒是強烈反前往緊張狀態下的國家。

金絲雀自身以國王和王妃的護衛這等條件得到了入境帝國的許可、不過、在神前決鬥中要由誰來擔任蘇菲和緹歐的護衛時、夏莉最先想起的是阿斯忒里歐所指導的隊伍。

「夏莉桑！！那個白癡皇帝是所有女人的敵人、斷他命根！」
「蕾亞、妳真的是⋯⋯決鬥的又不是皇帝陛下」

凱爾姑且勸告正在揮拳、憤概的蕾亞。

既然已經委託了護衛、那身份訊息公開就是無可避免的。先不說結界特化的專家阿斯忒里歐、要論能力的話肯定有更優秀的人、不過、選擇了沒有任何立場和瞭解自己的他們、夏莉判斷蘇菲和緹歐的血統應該不會流傳出去。
四人裡面的三人仍然是孩子般的新米冒險者、剩下一位是對權益不感興趣的僧兵、為人也無可挑惕。

「瞄準那傢伙的褲襠！！偷桃啊、猴子偷桃啊！！」
「記得在傷口上灑一點鹽和辣椒混合的東西、讓他痛快一下呀！」

在聽完夏莉的過去後、從反應上看來應該不會背叛自己吧。但、他們說的話對夏莉來說稍微有點下流、只能默默的紅著臉聽完、但願旁邊的人聽到不會讓他們被追究不敬之罪。

「但、吾輩感到很意外呢。愛德華陛下不說、連魔女閣下都如此放縱對方⋯⋯⋯⋯這是連吾輩們也無法知道的意圖？」

夏莉並沒有回答那個問題。只是、在進入競技場前、金絲雀對著夏莉、用手指了一指城堡、然後用大拇指比出一個讚、一邊用燦爛無比的笑容對著夏莉說。

『順便噠。讓那個低能說屁話的男孩吃點苦頭噠。汝也想一吐至今為止的憤怒吧？』

夏莉非常能夠了解那句話的涵義、自己也經常被金絲雀搞得烏煙瘴氣的、但、現在那些事情都無所謂了。

「⋯⋯母親⋯」

緹歐抓住夏莉連衣裙的裙擺。

「加油⋯⋯！」

蘇菲緊握即將前往決鬥場母親的手、看著母親。
自己手中的小手在顫抖著、眼神中透露著不安、但仍拚命掩飾心中的不安來目送自己母親的兩名女兒⋯⋯靜靜的⋯⋯⋯⋯夏莉心中、燃起了前所未見的高昂鬥志。

「沒問題的⋯⋯我一定會獲勝、然後回來的」

平常總是冷酷的夏莉、臉上出現少見淡淡的微笑、與蘇菲和緹歐的視線交會、慈祥溫柔的撫摸兩人的臉頰與頭髮。
目送瀟灑的離開自己、走向光芒四射的決鬥場大門、看向那母親的背影、只能用短暫又笨拙的吼叫來傳達各種情感與應援。

沒有理由。很單純的、對於父母⋯這樣就足以成為無敵的。

───

另一方面、與夏莉所在地相反的競技場內通道、代表帝國戰士的璐蜜安娜用頭繩把紅色的頭髮紮起來、手中緊握傳家之寶的魔劍。

看著身為自己摯友又是直屬騎士少女的背影、回想起到現在所有的經過、不由得咬牙切齒。
神前對決的性質、選出國家第一的騎士是理所然的⋯⋯然而現在的問題是、璐蜜安娜便是那年紀輕輕就取得帝國第一的騎士。
即使自己不是很瞭解劍術、但能夠被奉為『守護劍姬』保護自己的立場上、菲莉亞理解她的強大。

因此、並不是懷疑夏莉的實力、而是無論如何也不想看見璐蜜安娜的勝利。
那麼、不也是可以故意讓她負傷出場嗎、不也是可以拜託她放水嗎、那也是沒辦法的。

首先、作為前者的理由、想要讓帝國放棄的話、就必須擊倒帝國最強的戰士⋯⋯用冒險者的方式來比喻的話、就像是要擊倒魔物群必須先取魔物之主首級一樣。
如果不這樣做、之後不管怎樣都會被追究合理性、然而相反的、如果知道王國裡有比帝國任何一個騎士都強的人存在、那誰都不會想去挑戰了。為了讓帝國方接受神前決鬥的結果合理性這是必要的。

後者的理由便是璐蜜安娜出生的雷納德世家、其家族血統代代傳承的一種詛咒。
雷納德家族自古來就是騎士世家。不容許一切的怠慢與妥協的風氣、甚至是依賴魔術來強制執行。

以血統為媒介、對一切事物都不手下留情的詛咒。平於給予自己在鑽研與責任上的恩惠、現在卻變成了災禍。
這種極為扎深堅固的詛咒難以解除、就算是君主的命令也難以抗拒。不過、要是能使用聖國神官所擅長的審查魔術『識破』的話⋯⋯不過那也是在這之前的事了。
正因為如此、菲利亞策劃著如何不讓璐蜜安娜上場的計畫、不過、正準備要實踐之前、突然出現在帝國的金絲雀────

『叫那個雷納德家的小姑娘出來就好噠。只要討伐她之後、剩下都是微不足道的雜魚而已』

躲在憤概的阿爾貝托與騎士們身後不斷的低下頭、在金絲雀煽風點火的指名挑戰下。無法反抗再次被憤怒支配思考的阿爾貝托命令、璐蜜安娜就必須要這樣出場。
姑且算是協力者的金絲雀、但菲莉亞了解其真正的目的是要在帝國領土內拓展冒險者公會的商業利益。但不管怎麼說、在這種時機進行宣傳、時間點真的很差。

「公主殿下、變成這樣也沒辦法了。之後就相信夏莉閣下吧」
「⋯⋯對不起、璐蜜安娜。本應該是要站在支持妳的立場才對⋯⋯⋯⋯」
「比起我的武績、公主殿下所重視的東西更為重要⋯⋯」

能夠坦率的理解狀況實在讓人難以歡欣鼓舞、不過對於眼前緊握手中魔劍、能理解自己劍士職責的璐蜜安娜來說、確實不見得是件壞事。
意識到不能讓她擔心、嘗試在表面掩飾自己的菲莉亞、那表情仍相當哀傷。

雖然早已盡力、但皇女還只有十七歲。經驗、權利和力量都不足夠、很多時候無法如願以償而被時事帶著走。更不用說是關係到自己最重要的人和她女兒們的未來。
只能期望王國的《白之劍姬劍鬼》能超越帝國的《守護劍姬》⋯⋯⋯⋯再後來、菲莉亞很快就意識到自己只不過個井底之蛙而已。

───

「一切到位了嗎？」
「非常順利。雖是有些老舊的裝置、但經過幾次修理整併後、已經確認全部完成運作」

愛麗絲俯視眼前的文官、滿意的點頭。

「可能會有些卑鄙呢⋯⋯」
「那是沒辦法的。雖然不認為會輸給那些冒險者、但如此鄙視我們⋯看來還是要有些萬一的準備呢」
「更重要的是為了帝國的未來。無倫如何、都必須要姊姊大人的女兒們來到帝國」

隨意的考慮這種不值得一提的事情、愛麗絲面露聖人一般的面容抱著阿爾貝托的胳膊。
本來、要用自己厭惡的姊姊的孩子安置在下任皇位繼承位上是非常討厭的事情。但、即使不在意側室的存在自己仍是沒有孩子、意識到現在的立場必須貫徹下去是必要的。
那麼、就只能忍耐了。就讓自己好好教導她們、什麼叫做皇女教育教育欺凌吧。說起來自己支援者的其中有一個兒子⋯⋯去強迫她們倆去跟奢侈、醜陋、肥胖的男人結婚就好了。

「但是、我很擔心呢。孩子們能夠適應嚴酷的王族世界嗎⋯⋯⋯⋯至少就由我來保護和指導吧」
「妳一直都沒變呢、愛麗絲。就是因為妳如此的溫柔、我才願意捨身保護妳的」

一邊享受內心的痛快、一邊不忘阿諛諂媚。愛麗絲滿足於眼神沈溺於自己、雙手抱緊自己的阿爾貝托的態度、眺望決鬥場下方隱藏的機關。

「想不到呢、在空中競技場下方居然有這種東西呢⋯⋯」
「啊啊。那可是過去黑暗的、專門為了貴族們享樂競賽而設置的呢⋯⋯」

為了防礙競技場上的劍闘士而在下方狹窄處陳放著、競技場內部設置了許多自動裝置的陷阱與魔術式的地雷。

徒有虛名的競技場、為了讓其接待貴族們、和其所期望的劍闘士獲勝、堂堂正正只不過歷史洪流中的謊言、是連菲莉亞都不知道的帝國陰暗面。
這次、機關發出了沉重的聲音、這些陷阱被用來襲擊想要守護孩子的母親。

「不過這樣一來阿爾貝托大人就邁出成為大陸馳名偉大皇帝的第一步呢」
「啊啊。之後只要有妳在身邊、我不管到哪都可以征服那裡的」

在這充滿愛恨情仇旋渦的競技場。作為帝國、為了帝國的未來。作為王國、為了守護國民與牽制他國。身為母親、為了抓住孩子的未來而展開的決鬥、現在開始了。